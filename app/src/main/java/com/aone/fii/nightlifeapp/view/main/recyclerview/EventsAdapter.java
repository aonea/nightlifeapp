package com.aone.fii.nightlifeapp.view.main.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aone.fii.nightlifeapp.R;
import com.aone.fii.nightlifeapp.data.model.Event;
import com.bumptech.glide.Glide;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

    private Context mContext;
    private List<Event> eventList;
    private OnEventClickListener mOnEventClickListener;

    public EventsAdapter(Context mContext, List<Event> eventList, OnEventClickListener onEventClickListener) {
        this.mContext = mContext;
        this.eventList = eventList;
        this.mOnEventClickListener = onEventClickListener;
    }

    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_item_event, viewGroup, false);

        return new EventViewHolder(itemView, mOnEventClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder eventViewHolder, int i) {
        Event thisEvent = eventList.get(i);
        eventViewHolder.tvTitle.setText(thisEvent.getTitle());
        eventViewHolder.tvDescription.setText(thisEvent.getDescription());
        eventViewHolder.tvTo.setText(thisEvent.getTo());
        eventViewHolder.tvFrom.setText(thisEvent.getFrom() + " -> ");

        Glide.with(mContext)
                .load(thisEvent.getImageLocation())
                .placeholder(R.drawable.ic_launcher_background)
                .centerCrop()
                .into(eventViewHolder.ivImage);
    }

    @Override
    public int getItemCount() {
        if (eventList != null) {
            return eventList.size();
        } else {
            return 0;
        }
    }

    public class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvTitle;
        public TextView tvDescription;
        public ImageView ivImage;
        public TextView tvTo;
        public TextView tvFrom;
        OnEventClickListener onEventClickListener;

        public EventViewHolder(@NonNull View itemView, OnEventClickListener onEventClickListener) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvEventTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            ivImage = itemView.findViewById(R.id.ivEventImage);
            tvTo = itemView.findViewById(R.id.tvTo);
            tvFrom = itemView.findViewById(R.id.tvFrom);
            itemView.setOnClickListener(this);
            this.onEventClickListener = onEventClickListener;
        }


        @Override
        public void onClick(View view) {
            onEventClickListener.onClick(getAdapterPosition());
        }
    }

    public interface OnEventClickListener {
        void onClick(int position);
    }

}
