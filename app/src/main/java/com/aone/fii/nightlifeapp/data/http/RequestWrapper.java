package com.aone.fii.nightlifeapp.data.http;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.aone.fii.nightlifeapp.data.model.Event;
import com.aone.fii.nightlifeapp.data.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequestWrapper {
    private final static String BASE_URL = "https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/Test/events";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");;
    private static OkHttpClient client = new OkHttpClient();

    public static void getEvents(Callback callback) throws IOException {
        String baseUrl = BASE_URL;

        Request request = new Request.Builder()
                .url(baseUrl)
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public static void uploadEventImage(Bitmap bitmap, Callback callback) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            byte[] bytes = getBytesFromBitmap(bitmap);
            JSONObject json = new JSONObject("{\"image\": \"" + Base64.encodeToString(bytes, Base64.NO_WRAP) + "\"}");
            Log.d("===", json.toString());

            RequestBody req = RequestBody.create(JSON, json.toString());

            Request request = new Request.Builder()
                    //TODO:here
                    .url("https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/Test/events/upload")
                    .post(req)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Call call = client.newCall(request);
            call.enqueue(callback);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RequestWrapper", "Other Error: " + e.getLocalizedMessage());
        }

    }

    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static void postEvent(String json) {
        AsyncTask.execute(() -> {
            RequestBody req = RequestBody.create(JSON, json);

            Request request = new Request.Builder()
                    .url("https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/Test/events")
                    .post(req)
                    .build();

            try {
                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();
                Log.d("===", response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    public static void uploadImage(Bitmap bitmap, Callback callback, String url) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            byte[] bytes = getBytesFromBitmap(bitmap);
            JSONObject json = new JSONObject("{\"image\": \"" + Base64.encodeToString(bytes, Base64.NO_WRAP) + "\"}");
            Log.d("===", json.toString());

            RequestBody req = RequestBody.create(JSON, json.toString());

            Request request = new Request.Builder()
                    //TODO:here
                    .url(url)
                    .post(req)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Call call = client.newCall(request);
            call.enqueue(callback);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RequestWrapper", "Other Error: " + e.getLocalizedMessage());
        }

    }

    public static void postLocation(String json) {
        AsyncTask.execute(() -> {
            RequestBody req = RequestBody.create(JSON, json);

            Log.d("===", json);

            Request request = new Request.Builder()
                    .url("https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/Test/locations")
                    .post(req)
                    .build();

            try {
                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();
                Log.d("===", response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    public static void postNewUser(String email, String uid) {
        AsyncTask.execute(() -> {
            User user = new User(
                    uid,
                    email,
                    new ArrayList<>()
            );
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Log.d("===", gson.toJson(user));

            RequestBody req = RequestBody.create(
                    JSON,
                    gson.toJson(user)
                    );

            Request request = new Request.Builder()
                    .url("https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/beta/users")
                    .post(req)
                    .build();

            try {
                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();
                Log.d("===", response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    public static void getLocationById(String id, Callback callback) {

        Request request = new Request.Builder()
                //TODO:here
                .url("https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/beta/locations/" + id)
                .build();

        OkHttpClient client = new OkHttpClient();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }
}
