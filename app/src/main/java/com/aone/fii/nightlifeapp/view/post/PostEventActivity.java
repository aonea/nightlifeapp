package com.aone.fii.nightlifeapp.view.post;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

import com.aone.fii.nightlifeapp.R;
import com.aone.fii.nightlifeapp.data.http.RequestWrapper;
import com.aone.fii.nightlifeapp.data.model.Event;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class PostEventActivity extends AppCompatActivity {

    private static final int REQUEST_LOAD_IMAGE = 1;

    EditText etTitle;
    EditText etDescription;
    CalendarView calendarBegin;
    CalendarView calendarEnd;
    TimePicker tpBegin;
    TimePicker tpEnd;
    ImageView imageView;
    Button btnBrowse;
    Button btnPost;

    Uri selectedImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_event);
        initViews();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_LOAD_IMAGE) {
            selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private void initViews() {
        etTitle = findViewById(R.id.etEventTitle);
        etDescription = findViewById((R.id.etDescription));
        calendarBegin = findViewById(R.id.cvStartDate);
        calendarEnd = findViewById(R.id.cvEndDate);
        tpBegin = findViewById(R.id.tpStart);
        tpEnd = findViewById(R.id.tpEnd);
        imageView = findViewById(R.id.ivPostEventImage);
        btnBrowse = findViewById(R.id.btnBrowse);
        btnPost = findViewById(R.id.btnPost);

        btnBrowse.setOnClickListener((l) -> {
            Intent i = new Intent(
                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            );

            startActivityForResult(i, REQUEST_LOAD_IMAGE);
        });

        btnPost.setOnClickListener((l) -> {
            //post image first
            try {
                RequestWrapper.uploadImage(MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage), new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.e("POST", "Big faulure");
                        e.printStackTrace();
                    }

                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                       String str = response.body().string();

                        try {
                            JSONObject jsonObject = new JSONObject(str);
                            String locationString = jsonObject.getString("Location");
                            buildEvent(locationString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, "ef2e98b354db407bad0767f72c0d5f3e");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void buildEvent(String locationString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yy-mm-dd");
        String begin = sdf.format(calendarBegin.getDate());
        String end = sdf.format(calendarEnd.getDate());

        Event event = new Event (
                etTitle.getText().toString(),
                etDescription.getText().toString(),
                begin + " " + tpBegin.getHour() + ":" + tpBegin.getMinute(),
                end + " " + tpEnd.getHour() + ":" + tpEnd.getMinute(),
                locationString
        );

        event.setGoing(new ArrayList<String>());
        event.setLocationID("ef2e98b354db407bad0767f72c0d5f3e");

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Log.d("===", gson.toJson(event));
        RequestWrapper.postEvent(gson.toJson(event));
    }
}
