package com.aone.fii.nightlifeapp.view.main;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.aone.fii.nightlifeapp.R;
import com.aone.fii.nightlifeapp.data.http.RequestWrapper;
import com.aone.fii.nightlifeapp.data.model.Event;
import com.aone.fii.nightlifeapp.view.main.recyclerview.EventsAdapter;
import com.aone.fii.nightlifeapp.view.post.PostEventActivity;
import com.aone.fii.nightlifeapp.view.post.PostLocationActivity;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements EventsAdapter.OnEventClickListener {

    private FloatingActionButton btnAdd;
    private RecyclerView recyclerView;
    private EventsAdapter eventsAdapter;
    //will dispatch to repository / viewmodel
    private List<Event> mEventList;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TODO: delete
        //startActivity(new Intent(this, PostLocationActivity.class));
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //fab
        btnAdd = findViewById(R.id.floatingActionButton);
        recyclerView = (RecyclerView) findViewById(R.id.rvEventList);

        btnAdd.setOnClickListener((l) -> {
            Intent intent = new Intent(this, PostEventActivity.class);
            startActivity(intent);
        });

        mEventList = new ArrayList<>();
        eventsAdapter = new EventsAdapter(this, mEventList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(eventsAdapter);

        try {
            RequestWrapper.getEvents(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("MainActivity", "The big failure");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        JSONArray events = new JSONArray(response.body().string());
                        for (int i = 0; i < events.length(); i++) {
                            Gson gson = new GsonBuilder().create();
                            Event event = gson.fromJson(events.get(i).toString(), Event.class);
                            mEventList.add(event);
                            runOnUiThread(() -> eventsAdapter.notifyDataSetChanged());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(int position) {
        RequestWrapper.getLocationById(
                mEventList.get(position).getLocationId(),
                new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.d("===", "Big failure");
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        //get address
                        String jsonString = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(jsonString);
                            String address = jsonObject.getString("address");
                            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + address);
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }
}
