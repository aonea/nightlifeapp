package com.aone.fii.nightlifeapp.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class User {
    private String id;
    private String email;
    @SerializedName("following_locations")
    private ArrayList<String> followingLocations;

    public User(String id, String email, ArrayList<String> followingLocations) {
        this.id = id;
        this.email = email;
        this.followingLocations = followingLocations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<String> getFollowingLocations() {
        return followingLocations;
    }

    public void setFollowingLocations(ArrayList<String> followingLocations) {
        this.followingLocations = followingLocations;
    }
}
