package com.aone.fii.nightlifeapp.data.model;

import java.util.List;

public class Location {
    private String id;
    private String name;
    private String address;
    private String ownerId;
    private String openHours;
    private String imageLocation;
    private List<String> events;

    public Location(String name, String address, String ownerId, String openHours, String imageLocation, List<String> events) {
        this.name = name;
        this.address = address;
        this.ownerId = ownerId;
        this.openHours = openHours;
        this.imageLocation = imageLocation;
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOpenHours() {
        return openHours;
    }

    public void setOpenHours(String openHours) {
        this.openHours = openHours;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    public List<String> getEvents() {
        return events;
    }

    public void setEvents(List<String> events) {
        this.events = events;
    }
}
