package com.aone.fii.nightlifeapp.data.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Event {

    @SerializedName("name")
    private String title;
    private String description;
    private String from;
    private String to;
    private String imageLocation;
    private List<String> going;
    @SerializedName("locationID")
    private String locationId;


    public Event(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Event(String title, String description, String from, String to, String imageLocation) {
        this.title = title;
        this.description = description;
        this.from = from;
        this.to = to;
        this.imageLocation = imageLocation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getImage() {
        return null;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    public List<String> getGoing() {
        return going;
    }

    public void setGoing(List<String> going) {
        this.going = going;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationID(String locationId) {
        this.locationId = locationId;
    }
}
