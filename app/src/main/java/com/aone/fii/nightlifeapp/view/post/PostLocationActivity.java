package com.aone.fii.nightlifeapp.view.post;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.aone.fii.nightlifeapp.R;
import com.aone.fii.nightlifeapp.data.http.RequestWrapper;
import com.aone.fii.nightlifeapp.data.model.Location;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class PostLocationActivity extends AppCompatActivity {

    private static final int REQUEST_LOAD_IMAGE = 1;

    private Button btnBrowse;
    private Button btnPost;
    private EditText etName;
    private EditText etAddress;
    private EditText etAbout;
    private EditText etOpenHours;
    private ImageView imageView;

    Uri selectedImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_location);
        initViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_LOAD_IMAGE) {
            selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    private void initViews() {
        btnBrowse = findViewById(R.id.btnBrowseLocation);
        btnPost = findViewById(R.id.btnPostLocation);
        etName = findViewById(R.id.etLocationTitle);
        etAbout = findViewById(R.id.etAbout);
        etAddress = findViewById(R.id.etAddress);
        etOpenHours = findViewById(R.id.etOpenHours);
        imageView = findViewById(R.id.ivLocation);

        btnBrowse.setOnClickListener((l) -> {
            Intent i = new Intent(
                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            );

            startActivityForResult(i, REQUEST_LOAD_IMAGE);
        });

        btnPost.setOnClickListener((l) -> {
            //post image first
            try {
                RequestWrapper.uploadImage(MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage), new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.e("POST", "Big faulure");
                        e.printStackTrace();
                    }

                    @TargetApi(Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String str = response.body().string();
                        Log.d("===location img upload", str);
                        try {
                            JSONObject jsonObject = new JSONObject(str);
                            String locationString = jsonObject.getString("Location");
                            buildLocation(locationString);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, "https://jq7b5d39f8.execute-api.us-west-2.amazonaws.com/Test/events/upload");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void buildLocation(String locationString) {
        Location location = new Location(
                etName.getText().toString(),
                etAddress.getText().toString(),
                "100",
                etOpenHours.getText().toString(),
                locationString,
                new ArrayList<String>()
        );

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Log.d("===", gson.toJson(location));
        RequestWrapper.postLocation(gson.toJson(location));
    }
}
